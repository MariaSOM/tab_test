#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_timovy_tabs.h"
#include "qmessagebox.h"
#include <QTabWidget>
#include <QDialog>

#include <qdialogbuttonbox.h>
#include <QFileInfo>
#include <QString>


class timovy_tabs : public QMainWindow
{
	Q_OBJECT

public:
	timovy_tabs(QWidget *parent = Q_NULLPTR);
	
public slots:
	void actionopen();
	void actionclose();

private:
	Ui::timovy_tabsClass *ui;
	QTabWidget *tabs;
	int index = 0;

	//funkkcie
	void pridatVTK();
};

