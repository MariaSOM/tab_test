/********************************************************************************
** Form generated from reading UI file 'timovy_tabs.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TIMOVY_TABS_H
#define UI_TIMOVY_TABS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_timovy_tabsClass
{
public:
    QAction *actionopen;
    QAction *actionclose;
    QWidget *centralWidget;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QMenuBar *menuBar;
    QMenu *menuFile;

    void setupUi(QMainWindow *timovy_tabsClass)
    {
        if (timovy_tabsClass->objectName().isEmpty())
            timovy_tabsClass->setObjectName(QString::fromUtf8("timovy_tabsClass"));
        timovy_tabsClass->resize(600, 400);
        actionopen = new QAction(timovy_tabsClass);
        actionopen->setObjectName(QString::fromUtf8("actionopen"));
        actionclose = new QAction(timovy_tabsClass);
        actionclose->setObjectName(QString::fromUtf8("actionclose"));
        centralWidget = new QWidget(timovy_tabsClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        centralWidget->setEnabled(true);
        centralWidget->setMinimumSize(QSize(600, 0));
        centralWidget->setMaximumSize(QSize(16777215, 360));
        verticalLayoutWidget = new QWidget(centralWidget);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(0, 0, 601, 361));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        timovy_tabsClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(timovy_tabsClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 600, 26));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        timovy_tabsClass->setMenuBar(menuBar);

        menuBar->addAction(menuFile->menuAction());
        menuFile->addAction(actionopen);
        menuFile->addAction(actionclose);

        retranslateUi(timovy_tabsClass);
        QObject::connect(actionopen, SIGNAL(triggered()), timovy_tabsClass, SLOT(actionopen()));
        QObject::connect(actionclose, SIGNAL(triggered()), timovy_tabsClass, SLOT(actionclose()));

        QMetaObject::connectSlotsByName(timovy_tabsClass);
    } // setupUi

    void retranslateUi(QMainWindow *timovy_tabsClass)
    {
        timovy_tabsClass->setWindowTitle(QApplication::translate("timovy_tabsClass", "timovy_tabs", nullptr));
        actionopen->setText(QApplication::translate("timovy_tabsClass", "open", nullptr));
        actionclose->setText(QApplication::translate("timovy_tabsClass", "close", nullptr));
        menuFile->setTitle(QApplication::translate("timovy_tabsClass", "File", nullptr));
    } // retranslateUi

};

namespace Ui {
    class timovy_tabsClass: public Ui_timovy_tabsClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TIMOVY_TABS_H
